import people_data from "../data/people_data.json";

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getAll(limit: number = 10, page: number = 1) {
    const end = limit * page;
    const start = end - limit;
    return people_data.slice(start, end);
  }

  searchPerson(keywords: string) {
    keywords = keywords.toLowerCase();
    const results = people_data.filter((p) => {
      if (p.title?.includes(keywords)) {
        return p;
      }
      if (p.company.includes(keywords)) {
        return p;
      }
      if (p.gender === keywords) {
        return p;
      }
    });

    return results;
  }
}
