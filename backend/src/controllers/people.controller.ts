import {
  JsonController,
  Get,
  HttpCode,
  NotFoundError,
  Param,
  QueryParam,
} from "routing-controllers";
import { PeopleProcessing } from "../services/people_processing.service";

const peopleProcessing = new PeopleProcessing();

@JsonController("/people", { transformResponse: false })
export default class PeopleController {
  @HttpCode(200)
  @Get("/all")
  getAllPeople(
    @QueryParam("limit") limit: number,
    @QueryParam("page") page: number
  ) {
    const people = peopleProcessing.getAll(limit, page);

    if (!people) {
      throw new NotFoundError("No people found");
    }

    return {
      data: people,
    };
  }

  @HttpCode(200)
  @Get("/:id")
  getPerson(@Param("id") id: number) {
    const person = peopleProcessing.getById(id);

    if (!person) {
      throw new NotFoundError("No person found");
    }

    return {
      data: person,
    };
  }

  @HttpCode(200)
  @Get("/search")
  searchPerson(@QueryParam("keywords") keywords: string) {
    const searchResults = peopleProcessing.searchPerson(keywords);

    if (!searchResults) {
      throw new NotFoundError("Search not found");
    }

    return {
      data: searchResults,
    };
  }
}
