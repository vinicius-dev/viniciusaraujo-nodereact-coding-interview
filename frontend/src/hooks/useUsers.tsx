import { useCallback, useEffect, useState } from "react";
import { IUserProps } from "../dtos/user.dto";
import { BackendClient } from "../clients/backend.client";

type useUsersHook = [
  IUserProps[],
  (keywords: any) => Promise<void>,
  boolean,
  string | undefined
];

const useUsers = (): useUsersHook => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [isLoading, setLoader] = useState<boolean>(false);
  const [error, setError] = useState<string | undefined>(undefined);

  const getUsers = useCallback(async () => {
    setLoader(true);
    try {
      const client = new BackendClient();
      const { data } = await client.getAllUsers();
      setUsers(data);
      setError(undefined);
    } catch (e) {
      setError(
        "An error occurred while rendering the client. Try again later."
      );
    } finally {
      setLoader(false);
    }
  }, []);

  const searchUser = useCallback(async (keywords) => {
    setLoader(true);
    try {
      const client = new BackendClient();
      const { data } = await client.searchUserByKeywords(keywords);
      setUsers(data);
      setError(undefined);
    } catch (e) {
      setError("Error searching user.");
    } finally {
      setLoader(false);
    }
  }, []);

  useEffect(() => {
    getUsers();
  }, [getUsers]);

  return [users, searchUser, isLoading, error];
};

export default useUsers;
