import React, { FC, useCallback } from "react";
import { RouteComponentProps } from "@reach/router";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";
import { Search } from "../components/search/search.component";
import useUsers from "../hooks/useUsers";

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, searchUser, isLoading, error] = useUsers();

  const handleSearch = useCallback(
    (value: string) => {
      searchUser(value);
    },
    [searchUser]
  );

  if (isLoading) {
    return (
      <div className="centered-block">
        <CircularProgress size="60px" />
      </div>
    );
  }

  if (!isLoading && error) {
    <div className="centered-block">{error}</div>;
  }

  return (
    <div style={{ paddingTop: "30px" }}>
      <Search onSearchChanges={handleSearch} />
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div>
          {users.length ? (
            users.map((user) => {
              return <UserCard key={user.id} {...user} />;
            })
          ) : (
            <div className="centered-block">Nothing found</div>
          )}
        </div>
      </div>
    </div>
  );
};
