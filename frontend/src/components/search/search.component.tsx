import { TextField } from "@material-ui/core";
import React, { useState } from "react";

type SearchProps = {
  onSearchChanges: (value: any) => void;
};

export const Search: React.FC<SearchProps> = ({ onSearchChanges }) => {
  const [value, setValue] = useState<any>();
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setValue(event.target.value);
    onSearchChanges(event.target.value);
  };

  return (
    <TextField
      key="search"
      id="outlined-basic"
      label="Search people"
      variant="outlined"
      value={value}
      onChange={handleChange}
      fullWidth
    />
  );
};
